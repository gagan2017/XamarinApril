﻿using Xamarin.Forms;

namespace ListExamplePdf
{
	public partial class ListExamplePdfPage : ContentPage
	{
		ScrollView scr = new ScrollView();
		TapGestureRecognizer tapgesture = new TapGestureRecognizer();
		public ListExamplePdfPage()
		{
			tapgesture.Tapped+= Tapgesture_Tapped;
			//InitializeComponent();
			layout = new StackLayout()
			{
				Padding = 6
			};
			foreach (var item in Constants.colors)
			{

				var con=new ColorView()
				{
					StyleId=item,
					ColorName = item
				};
				con.GestureRecognizers.Add(tapgesture);
				layout.Children.Add(con);
			}
			scr.Content = layout;
			Content = scr;
		}

		async void Tapgesture_Tapped(object sender, System.EventArgs e)
		{
			var colorview = (ColorView)sender;
			var answer = await DisplayAlert(colorview.ColorName, "You have pressed "+colorview.ColorName, "Yes", "No");

		}
	}
}
