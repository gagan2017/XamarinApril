﻿using System;
using System.Collections.Generic;

namespace ListExamplePdf
{
	public static class Constants
	{
		public static List<string> colors;
		static Constants()
		{
			if (colors == null)
			{
				/*
		<local:ColorView ColorName="Aqua" />
		<local:ColorView ColorName="Black" />
		<local:ColorView ColorName="Blue" />
		<local:ColorView ColorName="Fuchsia" />
		<local:ColorView ColorName="Gray" />
		<local:ColorView ColorName="Green" />
		<local:ColorView ColorName="Lime" />
		<local:ColorView ColorName="Maroon" />
		<local:ColorView ColorName="Navy" />
		<local:ColorView ColorName="Olive" />
		<local:ColorView ColorName="Purple" />
		<local:ColorView ColorName="Pink" />
		<local:ColorView ColorName="Red" />
		<local:ColorView ColorName="Silver" />
		<local:ColorView ColorName="Teal" />
		<local:ColorView ColorName="White" />
		<local:ColorView ColorName="Yellow" />
			*/
				colors = new List<string>();
				colors.Add("Aqua");
				colors.Add("Black");
				colors.Add("Blue");
				colors.Add("Gray");
				colors.Add("Green");
				colors.Add("Lime");
				colors.Add("Maroon");
				colors.Add("Navy");
				colors.Add("Olive");
				colors.Add("Purple");
				colors.Add("Pink");
				colors.Add("Red");
				colors.Add("Silver");
				colors.Add("Teal");
				colors.Add("White");
				colors.Add("Yellow");
			}
		}
	}
}
