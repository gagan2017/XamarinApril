﻿using Xamarin.Forms;

namespace PlatformSpecific
{
	public partial class PlatformSpecificPage : ContentPage
	{
		public PlatformSpecificPage()
		{
			InitializeComponent();

			IPlatformInfo platformInfo = DependencyService.Get<IPlatformInfo>();
			modelLabel.Text = platformInfo.GetModel();
			versionLabel.Text = platformInfo.GetVersion();
		}
	
	}
}
