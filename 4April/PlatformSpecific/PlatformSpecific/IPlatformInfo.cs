﻿using System;
namespace PlatformSpecific
{
	
	public interface IPlatformInfo
	{
		string GetModel();
		string GetVersion();
	}
}
