﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Appstyles
{
	public partial class Page2_Yes : ContentPage
	{
		public Page2_Yes()
		{
			InitializeComponent();

		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			var main = new Message();
			MessagingCenter.Send<Message>(main, "Change");
			Navigation.PopAsync();		}
	}
}
