﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Game_Color
{
	public partial class Game_ColorPage : ContentPage
	{
		bool flag=true;
		TapGestureRecognizer tapGesture = new TapGestureRecognizer();
		Button Start;
		List<BoxView> boxview = new List<BoxView>();
		public Game_ColorPage()
		{
			Sessions.SetFavColor(Color.Red);
			CreateBoxes _cb = new CreateBoxes(ColorList.getColors());
			tapGesture.Tapped += OnBoxViewTapped;

			boxview = _cb.getBox();
			StackLayout layout = new StackLayout();
			foreach (var item in boxview)
			{
				item.GestureRecognizers.Add(tapGesture);
				layout.Children.Add(item);
			}
			Start = new Button();
			Start.Text = "Start";
			Start.Clicked += Start_Clicked;
			layout.Children.Add(Start);
			Content = layout;
			//InitializeComponent();
		}


		void OnBoxViewTapped(object sender, System.EventArgs e)
		{
			flag = false;
			var _boxView = (BoxView)sender;
			if (_boxView.Color == Sessions.getFavColor())
			{
				;
			}
			else
			{

			}
		}

		void Start_Clicked(object sender, System.EventArgs e)
		{

				while (flag == true)
				{

					HashSet<int> randomNumbers = RandomNumbers.getRandomNumber(ColorList.getColors().Count);

					var rndNo = Convertion.ConverToIntArray(randomNumbers);

					for (int i = 0; i < boxview.Count; i++)
					{
						boxview[i].Color = ColorList.getColors()[rndNo[i]];
					}

					Task.Delay(1000);

		}


	}
}
