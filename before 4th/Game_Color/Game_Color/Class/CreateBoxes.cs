﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Game_Color
{
	public class CreateBoxes
	{
		List<BoxView> _boxViewList;
		public CreateBoxes(List<Color> boxColors)
		{
			_boxViewList = new List<BoxView>();
			foreach(var item in  boxColors)
			{

				_boxViewList.Add(new BoxView()
				{
					WidthRequest = 100,
					HeightRequest = 100,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					Color = item

				});
			}
		}
		public List<BoxView> getBox()
		{
			return _boxViewList;
		}
	}
}
