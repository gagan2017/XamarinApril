﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Game_Color
{
	public static class ColorList
	{
		static List<Color> _color;
		static ColorList()
		{
			if (_color == null)
			{
				_color = new List<Color>();

				_color.Add(Color.Red);
				_color.Add(Color.Blue);
				_color.Add(Color.Green);
			}
		}
		public static List<Color> getColors()
		{
			return _color;
		}
	}
}
