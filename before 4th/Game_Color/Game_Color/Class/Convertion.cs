﻿using System;
using System.Collections.Generic;

namespace Game_Color
{
	public static class Convertion
	{
		public static int[] ConverToIntArray(HashSet<int> pSend)
		{
			int[] returnno = new int[pSend.Count];
			int i = 0;
			foreach (var item in pSend)
			{
				returnno[i] = item;
				i++;
			}
			return returnno;
		}
	}
}
