﻿using System;
using System.Collections.Generic;

namespace Game_Color
{
	public static class RandomNumbers
	{
		public static HashSet<int> getRandomNumber(int size)
		{
			Random rnd = new Random();

			var Out = new HashSet<int>();
			while (Out.Count != size)
			{

				Out.Add(rnd.Next(0, size));


			}
			return Out;
		}
	}
}
