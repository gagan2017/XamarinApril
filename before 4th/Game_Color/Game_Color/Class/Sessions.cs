﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Game_Color
{
	public static class Sessions
	{
		static string FavColor = "FavoriteColor";
		public static void SetFavColor(Color c)
		{
			Application.Current.Properties[FavColor] = c;

		}
		public static Color getFavColor()
		{
			IDictionary<string, object> properties = Application.Current.Properties;
		
			if (properties.ContainsKey(FavColor))
			{
				return (Color) properties[FavColor];
			}
			return Color.Black;
		}
	}
}
