﻿using System;
namespace Breverage
{
	public class AddIon2 : AddIonBrewerage
	{
		Brewerage bw;

		public AddIon2(Brewerage bw)
		{
			this.bw = bw;
		}
		public override int Cost()
		{
			return this.bw.Cost() + 20;
		}
	}
}
