﻿using System;
namespace Breverage
{
	public class AddIon3 : AddIonBrewerage
	{
		Brewerage bw;

		public AddIon3(Brewerage bw)
		{
			this.bw = bw;
		}
		public override int Cost()
		{
			return this.bw.Cost() + 30;
		}
	}
}
