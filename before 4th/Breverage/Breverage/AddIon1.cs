﻿using System;
namespace Breverage
{
	public class AddIon1 : AddIonBrewerage
	{
		Brewerage bw;

		public AddIon1(Brewerage bw)
		{
			this.bw = bw;
		}
		public override int Cost()
		{
			return this.bw.Cost() + 10;
		}
	}
}
