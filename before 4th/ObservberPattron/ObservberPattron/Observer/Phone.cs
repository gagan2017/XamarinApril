﻿using System;
namespace ObservberPattron
{
	public class Phone : IObserver,IDisplay
	{
		Weather _weather;
		public Phone(Weather w)
		{
			this._weather = w;
		}

		public void Display()
		{
			Console.WriteLine("temprature has changed from Phone current temp value is "+_weather.getTemprature());
		}

		public void Update()
		{
			Display();
		}
	}
}
