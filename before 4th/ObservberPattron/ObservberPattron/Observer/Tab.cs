﻿using System;
namespace ObservberPattron
{
	public class Tab : IObserver, IDisplay
	{
		Weather _weather;
		public Tab(Weather w)
		{
			this._weather = w;
		}

		public void Display()
		{
			Console.WriteLine("temprature has changed from Tab current temp value is " + _weather.getTemprature());
		}

		public void Update()
		{
			Display();
		}
	}
}
