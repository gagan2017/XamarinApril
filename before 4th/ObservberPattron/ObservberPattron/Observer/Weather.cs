﻿using System;
using System.Collections.Generic;

namespace ObservberPattron
{
	public class Weather : IObservable
	{
		float temprature;
		private float Temprature
		{
			get { return temprature; }
			set { temprature = value; Notify();}
		}
		public float getTemprature()
		{
			return Temprature;
		}
		public void calculatetemp(int i)
		{
			Temprature = 30*i;
		}
		List<IObserver> _listOfObserver = new List<IObserver>();
		public void Add(IObserver iob)
		{
			_listOfObserver.Add(iob);
		}

		public void Notify()
		{
			foreach (var item in _listOfObserver)
			{
				item.Update();
			}
		}

		public void Remove(IObserver iob)
		{
			_listOfObserver.Remove(iob);
		}
	}
}
