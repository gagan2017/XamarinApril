﻿using System;
namespace ObservberPattron
{
	public class Devices 
	{
		IObserver iobserver;
		IDisplay idisplay;
		public Devices(IObserver iobserver, IDisplay idisplay)
		{
			this.idisplay = idisplay;
			this.iobserver = iobserver;
		}
		public void Display()
		{
			idisplay.Display();
		}
		public void Update()
		{
			iobserver.Update();
		}
	}
}
