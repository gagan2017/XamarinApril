﻿using System;
using DefiningBindableProperty;
using ObservberPattron;
using Xamarin.Forms;

namespace Xamarin.FormsBook.Toolkit
{
	public class AltLabel : Label,IObserver
	{
		public AltLabel()
		{
			Sessions.sliders.Add(this);
		}
		public static readonly BindableProperty PointSizeProperty= BindableProperty.Create(
			"PointSize",typeof(double),typeof(AltLabel),8.0,propertyChanged: OnPointSizeChanged);
		public double PointSize
		{
			set { SetValue(PointSizeProperty, value); }
			get { return (double)GetValue(PointSizeProperty); }
		}
		static void OnPointSizeChanged(BindableObject bindable, object oldValue, object newValue)
		{

			((AltLabel)bindable).OnPointSizeChanged((double)oldValue, (double)newValue);
		}

		public void Update()
		{
			FontSize = Sessions.sliders.getValue() * PointSize / 72;
		}

		void OnPointSizeChanged(double oldValue, double newValue)
		{
			SetLabelFontSize(newValue);
		}
		void SetLabelFontSize(double pointSize)
		{
			FontSize = 160 * pointSize/72;
		}
	}
}
