﻿using System;
using System.Collections.Generic;

namespace ObservberPattron
{
	public class Sliders : IObservable
	{
		public static Sliders _instance=new Sliders();
		private Sliders()
		{
		}
		public static Sliders GetInstance()
		{
			return _instance;
		}
		double _sliderValue;
		private double _SliderValue
		{
			get { return _sliderValue; }
			set { _sliderValue = value; Notify();}
		}
		public double getValue()
		{
			return _SliderValue;
		}
		public void setValue(double value)
		{
			_SliderValue=value;
		}

		List<IObserver> _listOfObserver = new List<IObserver>();
		public void Add(IObserver iob)
		{
			_listOfObserver.Add(iob);
		}

		public void Notify()
		{
			foreach (var item in _listOfObserver)
			{
				item.Update();
			}
		}

		public void Remove(IObserver iob)
		{
			_listOfObserver.Remove(iob);
		}
	}
}
