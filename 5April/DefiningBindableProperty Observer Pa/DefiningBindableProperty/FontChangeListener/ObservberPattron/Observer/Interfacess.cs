﻿using System;
namespace ObservberPattron
{
	public interface IObservable
	{
		void Add(IObserver iob);
		void Remove(IObserver iob);
		void Notify();
	}
	public interface IObserver
	{
		void Update();
	}
	public interface IDisplay
	{
		void Display();
	}
}
