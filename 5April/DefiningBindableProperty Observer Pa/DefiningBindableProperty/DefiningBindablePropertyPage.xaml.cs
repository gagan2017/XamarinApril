﻿using ObservberPattron;
using Xamarin.Forms;
using Xamarin.FormsBook.Toolkit;
namespace DefiningBindableProperty
{
	public partial class DefiningBindablePropertyPage : ContentPage
	{
		public DefiningBindablePropertyPage()
		{

			InitializeComponent();
			slider.Value = (Sessions.AppTextSize-160)/100;
			for (int i = 4; i < 9; i++)
			{
				layout.Children.Add(new AltLabel()
				{
					Text = "Text of " + i + " points",
					PointSize = i
				});
			}
			Content = layout;

		}

		void Handle_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
		{
			Sessions.AppTextSize = slider.Value * 100 + 160;
		}
	}
}
