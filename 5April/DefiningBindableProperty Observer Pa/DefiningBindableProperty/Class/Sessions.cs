﻿using System;
using System.Collections.Generic;
using ObservberPattron;
using Xamarin.Forms;

namespace DefiningBindableProperty
{
	public static class Sessions
	{
		public static IDictionary<string, object> properties = Application.Current.Properties;

		public static  Sliders sliders = Sliders.GetInstance();
		public static double AppTextSize
		{

			get
			{
				if (properties.ContainsKey(Keys.Key_Text_Size))
				{
					sliders.setValue((double)properties[Keys.Key_Text_Size]);
						return (double)properties[Keys.Key_Text_Size];

				}
				return 0;
			}
			set { Application.Current.Properties[Keys.Key_Text_Size] = value;
				sliders.setValue(value );
				}
		}

	}
}
