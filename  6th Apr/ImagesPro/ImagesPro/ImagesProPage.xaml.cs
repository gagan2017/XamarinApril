﻿using Xamarin.Forms;

namespace ImagesPro
{
	public partial class ImagesProPage : ContentPage
	{
		CreateButton ImageFromInternet, ImageFromBitMap;
		ActivityIndicator indicator;
		public ImagesProPage()
		{
			InitializeComponent();

			ImageFromInternet = new CreateButton("Image From Internet");
			ImageFromInternet.Clicked += Handle_Clicked_ImageFromInternet;
			ImageFromBitMap = new CreateButton("Image From BitMap");
			ImageFromBitMap.Clicked+= ImageFromBitMap_Clicked;
			indicator = new ActivityIndicator()
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Color = Color.Black,
				IsVisible = false

			};
			layout.Children.Add(ImageFromInternet);
			layout.Children.Add(ImageFromBitMap);
			layout.Children.Add(indicator);
			Content = layout;
		}

		async void Handle_Clicked_ImageFromInternet(object sender, System.EventArgs e)
		{
			await this.Navigation.PushAsync(new ImageFromInternet());
		}

		async void ImageFromBitMap_Clicked(object sender, System.EventArgs e)
		{
			await this.Navigation.PushAsync(new ImageFromBitMap());
		}
	}
}
