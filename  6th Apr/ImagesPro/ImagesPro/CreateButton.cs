﻿using System;
using Xamarin.Forms;

namespace ImagesPro
{
	public class CreateButton :Button
	{
		
		public CreateButton(string Text)
		{
			this.HorizontalOptions = LayoutOptions.CenterAndExpand;
			this.VerticalOptions = LayoutOptions.CenterAndExpand;
			this.Text = Text;
		}
	}
}
