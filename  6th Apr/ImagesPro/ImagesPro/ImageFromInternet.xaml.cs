﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ImagesPro
{
	public partial class ImageFromInternet : ContentPage
	{
		ActivityIndicator indicator;
		public ImageFromInternet()
		{
			indicator = new ActivityIndicator()
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Color = Color.Blue,
				IsVisible = true

			};

			InitializeComponent();
			layout.Children.Add(indicator);

			image.BackgroundColor = Color.Gray;
				Content = layout;
		}


		void Handle_SizeChanged(object sender, System.EventArgs e)
		{
			indicator.IsVisible = false;
			indicator.IsRunning = false;
		}
	}
}
