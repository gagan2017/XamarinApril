﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ImagesPro
{
	public partial class ImageFromBitMap : ContentPage
	{
		public ImageFromBitMap()
		{
			InitializeComponent();
			Content = new Image
			{
				Source = ImageSource.FromResource(
							"ImagesPro.Images.MyPic.jpg"),
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center
			};
		}
	}
}
